{
  description = "An example of reproducible docker images";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    systems.url = "github:nix-systems/default";
  };

  outputs = inputs @ { self, flake-parts, ... }: flake-parts.lib.mkFlake { inherit inputs; } {
    systems = import inputs.systems;

    perSystem = { config, self', inputs', pkgs, system, lib, ... }: {
      packages.composer = pkgs.dockerTools.buildLayeredImage {
        name = "Composer";
        tag = pkgs.php.packages.composer.version;
        config = {
          Entrypoint = [ (lib.getExe pkgs.php83.packages.composer) ];
        };
        extraCommands = "mkdir -m 0777 tmp";
      };

      packages.plantuml-server = pkgs.dockerTools.buildLayeredImage {
        name = "PlantUMLServer";
        tag = "latest";
        config = {
          Env = [
            "PLANTUML_LIMIT_SIZE=4096"
            "GRAPHVIZ_DOT=${pkgs.graphviz}/bin/dot"
          ];

          Cmd = [ (lib.getExe config.packages.run-plantuml-server) ];

          ExposedPorts = {
            "8080/tcp" = {};
          };
        };
        extraCommands = "mkdir -m 0777 tmp";
      };

      packages.run-plantuml-server = pkgs.writeShellApplication {
        name = "run-plantuml-server";
        text = ''
          java -jar ${pkgs.jetty_11}/start.jar --module=deploy,http,jsp jetty.home=${pkgs.jetty_11} jetty.base=${./plantuml} jetty.http.host=0.0.0.0 jetty.http.port=8080
        '';
        runtimeInputs = [
          pkgs.jdk
          pkgs.jetty_11
        ];
      };

      apps.plantuml-server = {
        type = "app";
        program = lib.getExe config.packages.run-plantuml-server;
      };

      devShells.default = pkgs.mkShell {
        shellHook = ''
          echo "Welcome to the default development shell. All the tools are available here."
        '';
        packages = [
          config.packages.run-plantuml-server
          pkgs.dive
          pkgs.php83
          pkgs.php83.packages.composer
        ];
      };

      devShells.demo = pkgs.mkShell {
        name = "dev shell Nodejs";
        packages = [
          pkgs.nodejs
          pkgs.mailhog
        ];
      };
    };
  };
}
