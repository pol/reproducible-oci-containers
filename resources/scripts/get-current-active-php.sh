#!/usr/bin/env nix-shell
#! nix-shell -i bash --pure
#! nix-shell -p bash cacert curl jq
#! nix-shell -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/b06ff4bf8f4ad900fe0c2a61fc2946edc3a84be7.tar.gz

curl -s https://www.php.net/releases/active | jq -r .[].[].version